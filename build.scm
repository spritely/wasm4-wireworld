;; Copyright (C) 2023 Robin Templeton
;; Copyright (C) 2023 Christine Lemmer-Webber
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;;    http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(use-modules (wasm assemble)
             (ice-9 binary-ports)
             (ice-9 match))

(load "wireworld.scm")
(load "load-world.scm")

(define* (build-cart-from-file world-filename cart-filename
                               #:key (autostart? #f))
  (call-with-output-file cart-filename
    (lambda (port)
      (put-bytevector port
                      (wat->wasm (make-wireworld-game
                                  #:world (load-world-file world-filename)
                                  #:autostart? autostart?)))))
  (format #t "Built ~a\n" cart-filename))


(define* (build-cart-from-world-name world-name
                                     #:key (autostart? #f))
  (build-cart-from-file (format #f "worlds~a~a.txt"
                                file-name-separator-string world-name)
                        (format #f "build~awireworld-~a.wasm"
                                file-name-separator-string world-name)
                        #:autostart? autostart?))

(for-each (match-lambda
            ((world-name autostart?)
             (build-cart-from-world-name world-name
                                         #:autostart? autostart?)))
          '(("adder" #f)
            ("splash" #t)
            ("intro" #t)
            ("blank" #f)))

