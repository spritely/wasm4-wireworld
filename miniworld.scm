;; Copyright (C) 2023 Christine Lemmer-Webber
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;;    http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

;;; This is our miniworld demo file ;)

(use-modules (ice-9 match)
             (wasm assemble)
             (ice-9 binary-ports)
             (rnrs bytevectors))

(load "data-utils.scm")
(load "wat-utils.scm")

;; (put 'func 'scheme-indent-function 1)

(define $smiley$ '(i32.const #x19a0))
(define smiley-data
  #vu8(#b11000011
       #b10000001
       #b00100100
       #b00100100
       #b00000000
       #b00100100
       #b10011001
       #b11000011))

;;; ============
;;; DON'T CHANGE
;;; ============
(define (our-game-v0)
  `(module
    ;; Copies pixels to the framebuffer.
    (import "env" "blit" (func $blit (param i32 i32 i32 i32 i32 i32)))

    ;; Define a smiley sprite
    (data ,$smiley$ ,smiley-data)

    (func (export "update")
      (call $blit ,$smiley$
            (i32.const 76) (i32.const 76)  ; draw at 76, 76
            (i32.const 8) (i32.const 8)    ; 8x8 sprite
            (i32.const 0)))))

(define smiley-text "\
..####..
.######.
##.##.##
##.##.##
########
##.##.##
.##..##.
..####..")


(define blank-text "\
........
........
........
........
........
........
........
........")

(define house-text "\
...##...
..#.##..
.#.#.##.
#.#.#.##
.######.
.#.#.##.
.###.##.
........")

(define smiley-bv (text->1bpp-sprite-bv smiley-text))
(define house-bv (text->1bpp-sprite-bv house-text))

(define electron-head-text
  "\
X##X
#.~.
#~~#
X##X")

(define electron-tail-text
  "\
XXXX
X~.X
X#~X
XXXX")

(define electron-head-bv
  (text->2bpp-sprite-bv electron-head-text))
(define electron-tail-bv
  (text->2bpp-sprite-bv electron-tail-text))

(define top-hat-text
  "\
........
........
........
..#~~~..
..###~..
..####..
..XXXX..
.######.")

(define top-hat-bv
  (text->2bpp-sprite-bv top-hat-text))


(define $house$ '(i32.const #x19a8))

(define $electron-head$ '(i32.const #x19b0))
(define $electron-tail$ '(i32.const #x19b6))
(define $top-hat$ `(i32.const ,(+ #x19b6 6)))

(define $palette0$ '(i32.const #x04))
(define $palette1$ '(i32.const #x08))
(define $palette2$ '(i32.const #x0c))
(define $palette3$ '(i32.const #x10))

(define $draw-colors$ '(i32.const #x14))

(define set-draw-colors `(i32.store16 ,$draw-colors$ (i32.const #x4321)))

(define* (our-game #:key
                   (house house-bv)
                   (smiley smiley-bv))
  `(module
    (import "env" "memory" (memory 1))
    ;; Copies pixels to the framebuffer.
    (import "env" "blit" (func $blit (param i32 i32 i32 i32 i32 i32)))
    ;; Draws a rectangle.
    (import "env" "rect" (func $rect (param i32 i32 i32 i32)))

    ;; Define a smiley sprite
    (data ,$smiley$ ,smiley)
    (data ,$house$ ,house)
    (data ,$electron-head$ ,electron-head-bv)
    (data ,$electron-tail$ ,electron-tail-bv)
    (data ,$top-hat$ ,top-hat-bv)

    (func (export "start")
      ;; set up palette
      (i32.store ,$palette0$ (i32.const #xe8e6e1))
      (i32.store ,$palette1$ (i32.const #xe69df5))
      (i32.store ,$palette2$ (i32.const #x7d1b8c))
      (i32.store ,$palette3$ (i32.const #x324366))
      ,set-draw-colors)

    (func (export "update")
      ,(call-blit $electron-tail$ 8 8 4 4 #:2bpp? #t)
      ,(call-blit $electron-head$ 12 8 4 4 #:2bpp? #t)
      ,(call-blit $electron-tail$ 20 8 4 4 #:2bpp? #t)
      ,(call-blit $electron-head$ 24 8 4 4 #:2bpp? #t)

      ,(call-blit $top-hat$ 76 (- 76 8) 8 8 #:2bpp? #t)

      (i32.store16 ,$draw-colors$ (i32.const 4))
      (call $rect
            (i32.const 4) (i32.const 8)
            (i32.const 4) (i32.const 4))
      (call $rect
            (i32.const 16) (i32.const 8)
            (i32.const 4) (i32.const 4))
      (call $rect
            (i32.const 28) (i32.const 8)
            (i32.const 4) (i32.const 4))

      ,(call-blit $smiley$ 76 76 8 8)
      ,(call-blit $smiley$ 42 42 8 8
                  #:flip-y? #t #:rotate? #t)
      ,(call-blit $house$ (+ 76 16) (+ 76 16) 8 8)

      ,set-draw-colors)))

(define* (try-game #:optional (game (our-game)))
  (call-with-output-file "our-game.wasm"
    (lambda (op)
      (put-bytevector op (wat->wasm game))))
  (system* "wasm4" "our-game.wasm"))

