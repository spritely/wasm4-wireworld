GUILE = guile
HOOT = $(HOME)/src/guile-hoot-updates

# ./pre-inst-env sh -c "cd .../wasm4-wireworld; make HOOT=$PWD all"
all:
	mkdir -p build
	$(GUILE) --no-auto-compile -L $(HOOT)/module -s build.scm

clean:
	rm -f build/wireworld-adder.wasm
	rm -f build/wireworld-splash.wasm
	rm -f build/wireworld-intro.wasm
	rm -f build/wireworld-blank.wasm

play: all
	wasm4 build/cart.wasm
