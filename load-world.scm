(use-modules (ice-9 match)
             (ice-9 textual-ports)
             (rnrs bytevectors))

(define (insert-cell! bv pos char offset)
  (define to-copy
    (match char
      (#\space #vu8(0 0 0 0))
      (#\#     #vu8(1 0 0 0))
      (#\@     #vu8(2 0 0 0))
      (#\*     #vu8(3 0 0 0))))
  (bytevector-copy! to-copy 0
                    bv (+ (* pos 4) offset) 4))

(define (insert-line! bv line world-size offset)
  (define line-len (string-length line))
  (do ((i 0 (1+ i)))
      ((or (= i line-len)             ; end of the line
           (= i world-size)))         ; end of the world
    (insert-cell! bv i (string-ref line i) offset)))

(define* (world-text->world-bv world-text #:key (world-size 40))
  (define world-lines (string-split world-text #\newline))
  (define world-bv (make-bytevector (* world-size world-size 4)))
  (do ((line-num 0 (1+ line-num))
       (lines world-lines (cdr lines))
       (line-offset 0 (+ line-offset (* world-size 4))))
      ((or (null? lines)                     ; end of the lines
           (= line-num world-size)))         ; end of the world
    (insert-line! world-bv (car lines) world-size line-offset))
  world-bv)

(define* (load-world-file world-filename #:key (world-size 40))
  (world-text->world-bv (call-with-input-file world-filename
                          get-string-all)
                        #:world-size world-size))
