# Wireworld

A [Wireworld](https://en.wikipedia.org/wiki/Wireworld) implementation
written in [GNU Guile Scheme](https://www.gnu.org/software/guile/) and
[WebAssembly Text](https://webassembly.org/) targeting
[Wasm-4](https://wasm4.org) fantasy console.

Wasm4-Wireworld is a project of the Spritely Institute built on the
occasion of the Spring Lisp Game Jam 2023.

## Building

This project uses [Guile-Hoot's](https://gitlab.com/spritely/guile-hoot)
Wasm toolchain. Build Hoot in its checkout directory by running:

```
./bootstrap.sh && ./configure && make
```

Next, still within the Hoot directory, build the cart by running:

```shell
./pre-inst-env sh -c "cd $WIREWORLD; make HOOT=$PWD all"
```

...with `$WIREWORLD` set to this project's source directory.

## Usage

Once built, run `build/cart.wasm` using your preferred Wasm-4
implementation. On GNU Guix, run:

```
guix shell wasm4 -- wasm4 build/wireworld-blank.wasm
```

(Or try one of the other two carts in the build directory for some
interesting prebuilt examples.)

The game is controlled using the mouse and gamepad button 1, with the
latter typically mapped to the "x" key on PC keyboards. Press
button-1/"x" to pause/unpause the game; pausing is useful for editing
and debugging. To edit the world-state, hover the mouse cursor over a
cell, and press the left mouse button to cycle through the available
states (in order: copper, electron head, electron tail, and empty), or
press the right mouse button to clear the cell.

## Links

- [Wireworld Gitlab site](https://gitlab.com/spritely/wasm4-wireworld):
  This project's repository; comments and contributions are welcome!
- [The Wireworld computer](https://www.quinapalus.com/wi-index.html):
  History and documentation about Wireworld, including many useful
  example patterns
- [Wirewold for Emacs](https://gitlab.com/dustyweb/wireworld-el):
  A Lisp-based Wireworld implementation by Christine Lemmer-Webber
- [Spritely Networked Communities Institute](https://spritely.institute/):
  News and information about the Spritely Institute and our other projects
