;; Copyright (C) 2023 Christine Lemmer-Webber
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;;    http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(use-modules (ice-9 match)
             (ice-9 binary-ports))

(define (text->1bpp-sprite-bv str)
  (let lp ((chars (delete #\newline (string->list str)))
           (done-bytes '())
           (byte #b00000000)
           (pos 0))
    (match chars
      ('()
       (u8-list->bytevector
        (reverse (cons byte done-bytes))))
      ((char rest ...)
       (let ((updated-byte (update-byte-for-char-1bpp byte char pos)))
         (if (= pos 7)
             (lp rest (cons updated-byte done-bytes)
                 #b00000000 0)
             (lp rest done-bytes
                 updated-byte (1+ pos))))))))

(define (update-byte-for-char-1bpp byte char pos)
  (if (eqv? char #\.)
      (logior byte (ash #b1 (- 7 pos)))
      byte))


(define (text->2bpp-sprite-bv str)
  (let lp ((chars (delete #\newline (string->list str)))
           (done-bytes '())
           (byte #b00000000)
           (pos 0))
    (match chars
      ('()
       (u8-list->bytevector
        (reverse (cons byte done-bytes))))
      ((char rest ...)
       (let ((updated-byte (update-byte-for-char-2bpp byte char pos)))
         (if (= pos 3)
             (lp rest (cons updated-byte done-bytes)
                 #b00000000 0)
             (lp rest done-bytes
                 updated-byte (1+ pos))))))))

(define (update-byte-for-char-2bpp byte char pos)
  (define chosen-bits
    (match char
      (#\. #b00)
      (#\~ #b01)
      (#\# #b10)
      (#\X #b11)))
  (logior byte (ash chosen-bits (* (- 3 pos) 2))))
