;; Copyright (C) 2023 Robin Templeton
;; Copyright (C) 2023 Christine Lemmer-Webber
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;;    http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

;; (put 'func 'scheme-indent-function 1)
;; (put 'w:func 'scheme-indent-function 4)

(use-modules (ice-9 match))

(define (w:_import module procedure signature)
  `(import ,module ,procedure ,signature))

(define-syntax-rule (w:import module procedure signature)
  (w:_import (symbol->string 'module)
             (symbol->string 'procedure)
             'signature))

(define-syntax-rule (w:imports (import-defn ...) ...)
  (list (w:import import-defn ...) ...))

(define *imports*
  (w:imports))

(define (w:_global id type value)
  `(global ,id ,type ,value))

(define-syntax-rule (w:global id type value)
  (w:_global 'id 'type 'value))

(define-syntax w:global-i32
  (syntax-rules ()
    ((_ id value) (w:_global 'id i32 (i32.const value)))))

;; FIXME: remember previous address/value and optionally compute next
;; available address
(define (w:_data expr value)
  `(data ,expr ,value))

(define-syntax w:data-i32
  (syntax-rules ()
    ((_ address value) (w:_data `(i32.const ,address) value))))

;; FIXME: alternative form with empty data for address calculation
;; (e.g. for $W0 and $W1)
(define (w:_global-data id type address value) ;unquote-splicing
  `((w:_global ,id ,type (i32.const ,address))
    (w:data (i32.const ,address) ,value)))

(define-syntax-rule (w:global-data id type address value)
  (w:_global-data 'id 'type address value))

(define (w:_start id)
  `(start ,id))

(define-syntax-rule (w:start id)
  (w:_start 'id))

(define (w:_export name kind desc)
  `(export ,name (,kind ,desc)))

(define-syntax-rule (w:export name kind desc)
  (w:_export name 'kind 'desc))

(define (w:_func id params results locals . body)
  (define (process-func-body x) x)
  `(func ,id
         ,@(map (lambda (x)
                  (match x
                    ((id type) `(param ,id ,type))))
                params)
         ,@(if (null? results) '() `((result ,@results)))
         ,@(map (lambda (x)
                  (match x
                    ((id type) `(local ,id ,type))))
                locals)
         ,@(process-func-body body)))

(define-syntax-rule (w:func id params results locals body ...)
  (w:_func 'id 'params 'results 'locals `body ...))

(define (w:_module . body)
  `(module ,@body))

(define-syntax-rule (w:module body ...)
  (w:_module body ...))

(define else 'else)

;;FIXME: unused
(define (w:_ofunc . rest)
  `(func ,@rest))

(define-syntax-rule (ofunc rest ...)
  (w:_ofunc `rest ...))

;; FIXME: remove
;; example: replace (local.set ...) with ,(wfnord), or ,(w:_fnord)
(define (w:_fnord)
  '(local.set $state (call $load-xy (global.get $W0) (local.get $j) (local.get $i))))
(define-syntax-rule (wfnord)
  (w:_fnord))
