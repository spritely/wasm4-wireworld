;; Copyright (C) 2023 Robin Templeton
;; Copyright (C) 2023 Christine Lemmer-Webber
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;;    http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(load "watmac.scm")
(load "wasm4-base.scm")
(load "data-utils.scm")
(load "wat-utils.scm")
(load "load-world.scm")

(use-modules (rnrs bytevectors))

(use-modules (ice-9 iconv))

;; Minor helper procedures
;; =======================

(define (i32 x)
  `(i32.const ,x))


;; Globals managed in scheme-land (which aren't data pointers)
;; ===========================================================
(define $world-size$ '(i32.const 40))
(define $sprite-size$ '(i32.const 4))
(define $screen-size$ '(i32.const 160))


;; Game data, sprites, etc
;; =======================

(define-syntax-rule (define-2bpp-sprite (text-id bv-id)
                      text)
  (begin
    (define text-id text)
    (define bv-id
      (text->2bpp-sprite-bv text-id))))

(define-2bpp-sprite (head-text head-bv)
  "\
X##X
#~.#
#~~#
X##X")

(define-2bpp-sprite (tail-text tail-bv)
  "\
XXXX
X#~X
X##X
XXXX")

(define-2bpp-sprite (copper-text copper-bv)
  "\
XXXX
XXXX
XXXX
XXXX")


;;; Data pointer globals
;;; ====================

;; Okay we're committing to all 2bpp sprites for now, so this is easy enough
;; to use to calculate those offsets
(define sprite-byte-len
  (bytevector-length head-bv))
(define (sprite-addr pos)
  (i32 (+ #x19a0 (* sprite-byte-len pos))))

(define $copper-sprite$
  (sprite-addr 0))
(define $head-sprite$
  (sprite-addr 1))
(define $tail-sprite$
  (sprite-addr 2))

;;; ==================
;;; Micro-instructions
;;; ==================
(define $draw-colors$ '(i32.const #x14))
(define set-draw-colors `(i32.store16 ,$draw-colors$ (i32.const #x4321)))


;;; ===================================
;;; Game section definitions start here
;;; ===================================

(define-syntax-rule (build-data (addr bv) ...)
  (list (list 'data addr bv) ...))

(define (game-data)
  (build-data
   ($copper-sprite$ copper-bv)
   ($head-sprite$ head-bv)
   ($tail-sprite$ tail-bv)))

(define* (game-state-globals #:key (world #vu8())
                             (autostart? #f))
  `(;; the wireworld
    (global $W0 (mut i32) (i32.const #x19b8))
    (global $W1 (mut i32) (i32.const 0))

    (global $DEAD i32 (i32.const 0))
    (global $CUPR i32 (i32.const 1))
    (global $HEAD i32 (i32.const 2))
    (global $TAIL i32 (i32.const 3))
    (global $MAX_STATE i32 (i32.const 4))

    ;; Initial configuration
    ,(w:data-i32 #x19b8 world)

    ;; FIXME: useless debugging code
    (global $FMT3I i32 (i32.const #x3000))
    ,(w:data-i32 #x3000 (string->bytevector "%d,%d\x00" "ASCII"))
    (global $SCRATCH i32 (i32.const #x300b))
    ,(w:data-i32 #x300b
                 #vu8(49 49 49 49 50 50 50 50 51 51 51 51))

    
    (global $game-mode (mut i32)
            ,(if autostart?
                 '(i32.const 1)
                 '(i32.const 0)))
    (global $play-mode i32 (i32.const 1))

    (global $previous-gamepad (mut i32) (i32.const 0))
    (global $previous-buttons (mut i32) (i32.const 0))

    (global $frame-count (mut i32) (i32.const 0))))

(define func:init
  (w:func $init () () ()
    ;; $W0 is initialized statically
    (global.get $W0)
    (i32.mul ,$world-size$ ,$world-size$)
    i32.add
    (i32.mul (i32.const 4))
    (global.set $W1)
    (i32.store (global.get $PALETTE0) (i32.const #xf0f0f0))
    (i32.store (global.get $PALETTE1) (i32.const #xe69df5))
    (i32.store (global.get $PALETTE2) (i32.const #x801c8f))
    (i32.store (global.get $PALETTE3) (i32.const #x324366))
    ,set-draw-colors))

(define func:start
  (w:func $start () () ()
    nop))

(define func:mod-ws
  ;; This isn't an actual modulus function; we need only worry about
  ;; $a=-1 and $a=$WS (other values are assumed to lie within [0, $WS))
  (w:func $mod-ws (($n i32))
          (i32)
          (($r i32))
    (if (i32.eq (local.get $n) (i32.const -1))
        (then (local.set $r (i32.add ,$world-size$ (local.get $n))))
        (else (if (i32.eq (local.get $n) ,$world-size$)
                  (then (local.set $r (i32.const 0)))
                  (else (local.set $r (local.get $n))))))
    (local.get $r)))

(define func:load-xy
  (w:func $load-xy (($w i32) ($x i32) ($y i32))
          (i32) ()
    (call $mod-ws (local.get $x))
    ,$world-size$
    i32.mul
    (call $mod-ws (local.get $y))
    i32.add
    (i32.mul (i32.const 4))
    (local.get $w)
    i32.add
    i32.load))

(define func:store-xy
  (w:func $store-xy (($w i32) ($x i32) ($y i32) ($v i32))
          () ()
    (call $mod-ws (local.get $x))
    ,$world-size$
    i32.mul
    (call $mod-ws (local.get $y))
    i32.add
    (i32.mul (i32.const 4))
    (local.get $w)
    i32.add
    (local.get $v)
    i32.store))

(define func:chn-xyoff
  (w:func $chn-xyoff (($w i32) ($x i32) ($xd i32) ($y i32) ($yd i32))
          (i32) ()
    (local.get $w)
    (local.get $x)
    (local.get $xd)
    i32.add
    (local.get $y)
    (local.get $yd)
    i32.add
    (call $load-xy)
    (global.get $HEAD)
    i32.eq))

;; count HEAD cells in chebyshev neighborhood
(define func:count-head-neighbors
  (w:func $count-head-neighbors (($w i32) ($x i32) ($y i32))
          (i32)
          (($r i32))
    (call $chn-xyoff (local.get $w)
          (local.get $x) (i32.const -1)
          (local.get $y) (i32.const -1))
    (call $chn-xyoff (local.get $w)
          (local.get $x) (i32.const 0)
          (local.get $y) (i32.const -1))
    (call $chn-xyoff (local.get $w)
          (local.get $x) (i32.const 1)
          (local.get $y) (i32.const -1))
    (call $chn-xyoff (local.get $w)
          (local.get $x) (i32.const 1)
          (local.get $y) (i32.const 0))
    (call $chn-xyoff (local.get $w)
          (local.get $x) (i32.const 1)
          (local.get $y) (i32.const 1))
    (call $chn-xyoff (local.get $w)
          (local.get $x) (i32.const 0)
          (local.get $y) (i32.const 1))
    (call $chn-xyoff (local.get $w)
          (local.get $x) (i32.const -1)
          (local.get $y) (i32.const 1))
    (call $chn-xyoff (local.get $w)
          (local.get $x) (i32.const -1)
          (local.get $y) (i32.const 0))

    i32.add
    i32.add
    i32.add
    i32.add
    i32.add
    i32.add
    i32.add
    (local.set $r)
    ;; (if (i32.gt_u (local.get $r) (i32.const 0))
    ;;     (then
    ;;      (i32.store (global.get $SCRATCH) (local.get $x))
    ;;      (i32.store (i32.add (i32.const 4) (global.get $SCRATCH)) (local.get $y))
    ;;      (i32.store (i32.add (i32.const 8) (global.get $SCRATCH)) (local.get $r))
    ;;      (call $tracef (global.get $FMT3I) (global.get $SCRATCH))))
    (local.get $r)))

(define func:world-tick
  (w:func $world-tick ()
          ()
          (($tem i32))
    (call $world-tick* (global.get $W0) (global.get $W1))
    ;; swap worlds
    (local.set $tem (global.get $W0))
    (global.set $W0 (global.get $W1))
    (global.set $W1 (local.get $tem))))

(define func:world-tick*
  (w:func $world-tick* (($ow i32) ($nw i32)) ()
          (($i i32)
           ($j i32)
           ($state i32)
           ($head-neighbors i32))
    (block
     (local.set $i (i32.const 0))
     (loop
      (local.get $i)
      ,$world-size$
      (i32.ge_u)
      (br_if 1)

      (block
       (local.set $j (i32.const 0))
       (loop
        (local.get $j)
        ,$world-size$
        (i32.ge_u)
        (br_if 1)

        (local.set $state (call $load-xy (local.get $ow) (local.get $j) (local.get $i)))
        (call $count-head-neighbors (local.get $ow)
              (local.get $j)
              (local.get $i))
        (local.set $head-neighbors)
        (if (i32.and (i32.eq (local.get $state) (global.get $CUPR))
                     (i32.or (i32.eq (local.get $head-neighbors)
                                     (i32.const 1))
                             (i32.eq (local.get $head-neighbors)
                                     (i32.const 2))))
            (then
             (call $store-xy
                   (local.get $nw)
                   (local.get $j)
                   (local.get $i)
                   (global.get $HEAD)))
            (else
             (if (i32.eq (local.get $state)
                         (global.get $HEAD))
                 (then (call $store-xy
                             (local.get $nw)
                             (local.get $j)
                             (local.get $i)
                             (global.get $TAIL)))
                 (else
                  (if (i32.eq (local.get $state) (global.get $TAIL))
                      (then (call $store-xy
                                  (local.get $nw)
                                  (local.get $j)
                                  (local.get $i)
                                  (global.get $CUPR)))
                      (else
                       (call $store-xy
                             (local.get $nw)
                             (local.get $j)
                             (local.get $i)
                             (local.get $state))))))))

        (local.get $j)
        (i32.const 1)
        i32.add
        (local.set $j)

        (br 0)))

      (local.get $i)
      (i32.const 1)
      i32.add
      (local.set $i)

      (br 0)))))

(define func:update
  (w:func $update () ()
          (($i i32) ($j i32) ($state i32) ($tem i32)
           ($gamepad i32) ($frame-gamepad i32)
           ($mouse-pos-x i32) ($mouse-pos-y i32)
           ($mouse-cell-x i32) ($mouse-cell-y i32)
           ($buttons i32) ($frame-buttons i32))

    ;; Store recent mouse and gamepad inputs
    (local.set $gamepad (i32.load8_u (global.get $GAMEPAD1)))
    (local.set $frame-gamepad
               (i32.and (local.get $gamepad)
                        (i32.xor (local.get $gamepad)
                                 (global.get $previous-gamepad))))
    (global.set $previous-gamepad (local.get $gamepad))

    (local.set $buttons (i32.load8_u (global.get $MOUSE_BUTTONS)))
    (local.set $frame-buttons
               (i32.and (local.get $buttons)
                        (i32.xor (local.get $buttons)
                                 (global.get $previous-buttons))))
    (global.set $previous-buttons (local.get $buttons))

    (local.set $mouse-pos-x (i32.load16_u (global.get $MOUSE_X)))
    (local.set $mouse-pos-y (i32.load16_u (global.get $MOUSE_Y)))

    (local.set $mouse-cell-x (i32.div_u (local.get $mouse-pos-x) ,$sprite-size$))
    (local.set $mouse-cell-y (i32.div_u (local.get $mouse-pos-y) ,$sprite-size$))

    (if (i32.and (local.get $frame-buttons) (global.get $MOUSE_RIGHT))
        (then
         (call $store-xy
               (global.get $W0)
               (local.get $mouse-cell-y)
               (local.get $mouse-cell-x)
               (global.get $DEAD)))
        (else
         (if (i32.and (local.get $frame-buttons) (global.get $MOUSE_LEFT))
             (then
              ;; Make sure mouse click is within screen bounds
              ;; ("hilariously", the html version permits out of bounds...)
              (if (i32.and (i32.le_u (local.get $mouse-cell-x) ,$screen-size$)
                           (i32.le_u (local.get $mouse-cell-y) ,$screen-size$))
                  (then
                   (call $store-xy
                         (global.get $W0)
                         (local.get $mouse-cell-y)
                         (local.get $mouse-cell-x)
                         (i32.rem_u
                          (i32.add
                           (i32.const 1)
                           (call $load-xy
                                 (global.get $W0)
                                 (local.get $mouse-cell-y)
                                 (local.get $mouse-cell-x)))
                          (global.get $MAX_STATE)))))))))
    (i32.store16 (global.get $DRAW_COLORS) (i32.const 2))
    (call $rect
          (i32.mul ,$sprite-size$ (local.get $mouse-cell-x))
          (i32.mul ,$sprite-size$ (local.get $mouse-cell-y))
          ,$sprite-size$
          ,$sprite-size$)
    ,set-draw-colors

    ;; Draw world state
    (local.set $i (i32.const 0))
    (loop $loopi
          (local.set $j (i32.const 0))

          (loop $loopj
                (local.set $state (call $load-xy (global.get $W0) (local.get $j) (local.get $i)))

                (if (i32.eq (local.get $state) (global.get $CUPR))
                    (then
                     ,(call-blit $copper-sprite$
                                 `(i32.mul ,$sprite-size$ (local.get $i))
                                 `(i32.mul ,$sprite-size$ (local.get $j))
                                 4 4 #:2bpp? #t)))

                (if (i32.eq (local.get $state) (global.get $TAIL))
                    (then
                     ,(call-blit $tail-sprite$
                                 `(i32.mul ,$sprite-size$ (local.get $i))
                                 `(i32.mul ,$sprite-size$ (local.get $j))
                                 4 4 #:2bpp? #t)))

                (if (i32.eq (local.get $state) (global.get $HEAD))
                    (then
                     ,(call-blit $head-sprite$
                                 `(i32.mul ,$sprite-size$ (local.get $i))
                                 `(i32.mul ,$sprite-size$ (local.get $j))
                                 4 4 #:2bpp? #t)))

                (local.get $j)
                ,$world-size$
                i32.lt_u

                (local.get $j)
                (i32.const 1)
                i32.add
                (local.set $j)

                (br_if $loopj))

          (local.get $i)
          ,$world-size$
          i32.lt_u

          (local.set $i (i32.add (local.get $i) (i32.const 1)))

          (br_if $loopi))

    (if (i32.and (local.get $frame-gamepad (global.get $BUTTON_1)))
        (then (global.set $game-mode
                          (i32.xor (global.get $game-mode)
                                   (i32.const 1)))))

    (if (i32.eq (global.get $game-mode) (global.get $play-mode))
        (then
         (if (i32.eq (global.get $frame-count) (i32.const 0))
             (then (call $world-tick)))))

    ;; speed control (10 "FPS")
    (global.set $frame-count
                (i32.rem_u (i32.add (global.get $frame-count)
                                    (i32.const 1))
                           (i32.const 10)))))


;; a procedure to allow easier live hacking / redefinition of individual
;; funcs
(define (game-funcs)
  (list func:init func:mod-ws
        func:load-xy func:store-xy func:chn-xyoff
        func:count-head-neighbors
        func:world-tick func:world-tick*
        func:start
        func:update))

(define game-exports
  (list
   (w:export "start" func $start)
   (w:start $init)
   (w:export "update" func $update)))

(define* (make-wireworld-game #:key (world #vu8())
                              (autostart? #f))
  `(module
    ,@wasm4-imports
    ,@wasm4-globals

    ,@(game-data)
    ,@(game-state-globals #:world world
                          #:autostart? autostart?)
    ,@(game-funcs)
    ,@game-exports))



;;; ==========
;;; Trial area
;;; ==========

(use-modules (ice-9 binary-ports)
             (wasm assemble))

(define* (try-game #:optional
                   (game (make-wireworld-game 
                          #:world
                          (load-world-file "worlds/splash.txt")
                          #:autostart? #t)))
  (call-with-output-file "our-game.wasm"
    (lambda (op)
      (put-bytevector op (wat->wasm game))))
  (system* "wasm4" "our-game.wasm"))

(define (try-world-file world-filename)
  (try-game (game (make-wireworld-game
                   #:world (load-world-file world-filename)))))
