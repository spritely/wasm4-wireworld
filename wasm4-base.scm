(load "watmac.scm")

(define wasm4-imports
  (list
   (w:import env memory (memory 1))
   ;; Copies pixels to the framebuffer.
   (w:import env blit (func $blit (param i32 i32 i32 i32 i32 i32)))
   ;; Copies a subregion within a larger sprite atlas to the framebuffer.
   (w:import env blitSub (func $blitSub (param i32 i32 i32 i32 i32 i32 i32 i32 i32)))
   ;; Draws a line between two points.
   (w:import env line (func $line (param i32 i32 i32 i32)))
   ;; Draws a horizontal line.
   (w:import env hline (func $hline (param i32 i32 i32)))
   ;; Draws a vertical line.
   (w:import env vline (func $vline (param i32 i32 i32)))
   ;; Draws an oval (or circle).
   (w:import env oval (func $oval (param i32 i32 i32 i32)))
   ;; Draws a rectangle.
   (w:import env rect (func $rect (param i32 i32 i32 i32)))
   ;; Draws text using the built-in system font.
   (w:import env text (func $text (param i32 i32 i32)))
   ;; Plays a sound tone.
   (w:import env tone (func $tone (param i32 i32 i32 i32)))
   ;; Reads up to `size` bytes from persistent storage into the pointer `dest`.
   (w:import env diskr (func $diskr (param i32 i32)))
   ;; Writes up to `size` bytes from the pointer `src` into persistent storage.
   (w:import env diskw (func $diskw (param i32 i32)))
   ;; Prints a message to the debug console.
   (w:import env trace (func $trace (param i32)))
   ;; Prints a message to the debug console.
   (w:import env tracef (func $tracef (param i32 i32)))))

(define wasm4-globals
  (list
   (w:global $PALETTE0 i32 (i32.const #x04))
   (w:global $PALETTE1 i32 (i32.const #x08))
   (w:global $PALETTE2 i32 (i32.const #x0c))
   (w:global $PALETTE3 i32 (i32.const #x10))
   (w:global $DRAW_COLORS i32 (i32.const #x14))
   (w:global $GAMEPAD1 i32 (i32.const #x16))
   (w:global $GAMEPAD2 i32 (i32.const #x17))
   (w:global $GAMEPAD3 i32 (i32.const #x18))
   (w:global $GAMEPAD4 i32 (i32.const #x19))
   (w:global $MOUSE_X i32 (i32.const #x1a))
   (w:global $MOUSE_Y i32 (i32.const #x1c))
   (w:global $MOUSE_BUTTONS i32 (i32.const #x1e))
   (w:global $SYSTEM_FLAGS i32 (i32.const #x1f))
   (w:global $NETPLAY i32 (i32.const #x20))
   (w:global $FRAMEBUFFER i32 (i32.const #xa0))

   (w:global $BUTTON_1 i32 (i32.const 1))
   (w:global $BUTTON_2 i32 (i32.const 2))
   (w:global $BUTTON_LEFT i32 (i32.const 16))
   (w:global $BUTTON_RIGHT i32 (i32.const 32))
   (w:global $BUTTON_UP i32 (i32.const 64))
   (w:global $BUTTON_DOWN i32 (i32.const 128))

   (w:global $MOUSE_LEFT i32 (i32.const 1))
   (w:global $MOUSE_RIGHT i32 (i32.const 2))
   (w:global $MOUSE_MIDDLE i32 (i32.const 4))

   (w:global $SYSTEM_PRESERVE_FRAMEBUFFER i32 (i32.const 1))
   (w:global $SYSTEM_HIDE_GAMEPAD_OVERLAY i32 (i32.const 2))

   (w:global $BLIT_2BPP i32 (i32.const 1))
   (w:global $BLIT_1BPP i32 (i32.const 0))
   (w:global $BLIT_FLIP_X i32 (i32.const 2))
   (w:global $BLIT_FLIP_Y i32 (i32.const 4))
   (w:global $BLIT_ROTATE i32 (i32.const 8))

   (w:global $TONE_PULSE1 i32 (i32.const 0))
   (w:global $TONE_PULSE2 i32 (i32.const 1))
   (w:global $TONE_TRIANGLE i32 (i32.const 2))
   (w:global $TONE_NOISE i32 (i32.const 3))
   (w:global $TONE_MODE1 i32 (i32.const 0))
   (w:global $TONE_MODE2 i32 (i32.const 4))
   (w:global $TONE_MODE3 i32 (i32.const 8))
   (w:global $TONE_MODE4 i32 (i32.const 12))
   (w:global $TONE_PAN_LEFT i32 (i32.const 16))
   (w:global $TONE_PAN_RIGHT i32 (i32.const 32))))
