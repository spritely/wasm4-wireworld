;; Copyright (C) 2023 Christine Lemmer-Webber
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;;    http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

;; General purpose WAT/WASM and WASM4 tools.
;; Not specific to Wireworld.

(define BLIT-FLAG-2BPP #b00000001)
(define BLIT-FLAG-FLIP-X #b00000010)
(define BLIT-FLAG-FLIP-Y #b00000100)
(define BLIT-FLAG-ROTATE #b00001000)

(define (maybe-i32.const x)
  (if (number? x)
      `(i32.const ,x)
      x))

(define* (call-blit sprite-ptr x y width height
                    #:key 2bpp? rotate? flip-x? flip-y?
                    (flags (logior #b0
                                   (if 2bpp? BLIT-FLAG-2BPP #b0)
                                   (if flip-x? BLIT-FLAG-FLIP-X #b0)
                                   (if flip-y? BLIT-FLAG-FLIP-Y #b0)
                                   (if rotate? BLIT-FLAG-ROTATE #b0))))
  `(call $blit ,sprite-ptr
         ,(maybe-i32.const x) ,(maybe-i32.const y)
         ,(maybe-i32.const width) ,(maybe-i32.const height)
         ,(maybe-i32.const flags)))
